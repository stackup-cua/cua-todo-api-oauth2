/**
 * Created by phangty on 26/9/16.
 */
var authController = require('./auth.js');
var clientController = require('./client.js');
var oauth2Controller = require('./oauth2.js');
var todoController = require('./todo.js');
var userController = require('./user.js');


module.exports.set = function(router, app) {

    router.route('/todos')
        .post(todoController.postTodos)
        .get(todoController.getTodos);

    router.route('/todos/:todo_id')
        .get(todoController.getTodo)
        .put(todoController.putTodo)
        .delete(todoController.deleteTodo);

    router.route('/users')
        .post(userController.postUsers)
        .get(authController.isAuthenticated, userController.getUsers);

    router.route('/clients')
        .post(authController.isAuthenticated, clientController.postClients)
        .get(authController.isAuthenticated, clientController.getClients);

    router.route('/oauth2/authorize')
        .get(authController.isAuthenticated, oauth2Controller.authorization)
        .post(authController.isAuthenticated, oauth2Controller.decision);

    router.route('/oauth2/token')
        .post(authController.isClientAuthenticated, oauth2Controller.token);

    app.use('/api', router);

};