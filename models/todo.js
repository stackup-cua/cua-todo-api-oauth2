/**
 * Created by phangty on 26/9/16.
 */
// Load required packages
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

// Define our client schema
var ToDoSchema = new mongoose.Schema({
    order: { type: Number, required: true },
    title: { type: String, required: true },
    completed: { type: Boolean},
    completionDate : { type : Date, default: Date.now },
    dueDate : { type : Date, default: Date.now },
    userId: { type: String, required: true }
}, {
    timestamps: true
});

ToDoSchema.plugin(mongoosePaginate);

// Export the Mongoose model
module.exports = mongoose.model('todo', ToDoSchema);
